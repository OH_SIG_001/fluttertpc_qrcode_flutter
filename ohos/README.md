# qrcode_flutter_ohos

Flutter plugin for scanning QR codes. Customize your scanning interface using PlatformView. Import images from the photo album for scanning.

## Usage
```pod
dependencies:
qrcode_flutter: ^1.0.0
qrcode_flutter_ohos: ^1.0.0
```

### Example

```pod
import 'package:qrcode_flutter_ohos/qrcode_flutter_ohos.dart';
import 'package:image_picker/image_picker.dart';
QRCaptureController _controller = QRCaptureController();

final ImagePicker picker = ImagePicker();
final XFile? image = await picker.pickImage(source: ImageSource.gallery);
if(image != null) {
   var qrCodeResult = await QRCaptureController.getQrCodeByImagePath(image.path);
}
_controller.pause();
_controller.resum();
_controller.setTorch();
_controller.setTorch();
_controller.onCapture((data) {
   print('$data');
}

```
